from email.mime.text import MIMEText
import smtplib

msg = MIMEText(
   "<h1>Titre</h1><p>Bonjour le monde !</p>","html")

msg['Subject'] = 'Test de message HTML'
msg['From']='SenderAddress'
msg['To'] = 'RecipientAddress'

s = smtplib.SMTP('localhost')
s.sendmail('SenderAddress',
           ['RecipientAddress'],
           msg.as_string())

print("Message envoyé !")
